import ursina as u
from pYTerm import pYTerm
from opensimplex import OpenSimplex
import threading
import time

u.window.vsync = False
# u.window.size = u.Vec2(640,540)
seed = 0
noisemaker = OpenSimplex(seed=seed)
player = pYTerm.Player(enable_input= False, legacystreams= True)
player.play_all(halting= False, keep_alive= True)


class CubeHolder(u.Entity):
    def __init__(self,gen, CubeAnimator):
        self.gen = gen
        self.animator = CubeAnimator
        self.gencubes = True
        super().__init__()
    def update(self):
        if self.gencubes:
            try:
                next(self.gen)
                # next(self.gen)
                # next(self.gen)
            except Exception as e:
                print('cubeholder err:',e)
                # self.combine()
                self.gencubes = False
        else:
            self.animator.update()

cubeHolder = None
cubes = []
grid = [14,8]
offset = [(grid[0]/2)-0.5,(grid[-1]/2)-0.5]
spacing = 1.3

def next_block():
    for x in range(grid[0]):
        truX = x
        x -= offset[0]
        x *= spacing
        y_cubes = []
        for y in range(grid[-1]):
            truY = y
            y -= offset[1]
            y *= spacing
            y_cubes.append(u.Entity(parent=cubeHolder, model='quad', x=x, y=y, scale=0.9, ignore = True))
            print('new at',x,y)
            yield
        cubes.append(y_cubes)

class cube_animator():
    def __init__(self,speed=0.05, wildness=2, spice=0.07, color=[255,1,1]):
        super().__init__()
        self.speed = speed
        self.color = color
        self.spice = spice
        self.wildness = wildness
        self.tickcounter=0
        self.animationcount = 0
        self.movex = 1
        self.movey = 1
        self.paused = False
        self.can_animate = False
        self.start()

    def pause(self):
        color = u.color.rgb(int(self.color[0]/3),int(self.color[1]/3),int(self.color[2]/3))
        if not self.paused:
            for x,yrow in enumerate(cubes):
                for y,block in enumerate(yrow):
                    # block.color = u.color.rgb(50,0,0)
                    block.animate_color(color, duration=0.5)
        self.paused = True

    def unpause(self):
        color = u.color.rgb(self.color[0],self.color[1],self.color[2])
        if self.paused:
            for x,yrow in enumerate(cubes):
                for y,block in enumerate(yrow):
                    # block.color = u.color.red
                    block.animate_color(color, duration=1)
        self.paused = False

    def toggle_pause(self):
        if self.paused:
            self.unpause()
        else:
            self.pause()

    def start(self):
        color = u.color.rgb(self.color[0],self.color[1],self.color[2])
        if not self.can_animate:
            for x,yrow in enumerate(cubes):
                for y,block in enumerate(yrow):
                    block.animate_color(color, duration=0.5)
            self.can_animate = True

    def stop(self):
        if self.can_animate:
            for x,yrow in enumerate(cubes):
                for y,block in enumerate(yrow):
                    block.animate_z(0, duration=0.5)
                    block.animate_color(u.color.white, duration=0.5)
            self.can_animate = False

    def update(self):
        if self.can_animate:
            if not self.paused:
                self.animate()

    def animate(self):
        count = self.animationcount
        for x,yrow in enumerate(cubes):
            for y,block in enumerate(yrow):
                noise = noisemaker.noise2d((x+count*self.movex)*self.spice,(y+count*self.movey)*self.spice)
                # print(block)
                block.z = self.wildness*noise
                clrnoise = 1-((noise+1)/2)
                color = u.color.rgb(int((self.color[0]/3)*2*clrnoise+self.color[0]/3),
                                    int((self.color[1]/3)*2*clrnoise+self.color[1]/3),
                                    int((self.color[2]/3)*2*clrnoise+self.color[2]/3))
                block.color = color
        self.animationcount += self.speed

class animationController():
    def __init__(self, animator):
        self.animator = animator
        self.speed_slider = u.Slider(label="speed", min=0,max=5,step=0.01, default=0.05)
        self.spice_slider = u.Slider(label="spice", min=0.01,max=1.5,step=0.01, default=0.07)
        self.wild_slider = u.Slider(label="wildness", min=0,max=5,step=0.01, default=2)
        self.movex_slider = u.Slider(label="movex", min=-1,max=1,step=0.5, default=1)
        self.movey_slider = u.Slider(label="movey", min=-1,max=1,step=0.5, default=1)
        self.r_slider = u.Slider(label="movey", min=1,max=225,step=1, default=225)
        self.g_slider = u.Slider(label="movey", min=1,max=225,step=1, default=1)
        self.b_slider = u.Slider(label="movey", min=1,max=225,step=1, default=1)
        self.r_slider.on_value_changed =self.update
        self.g_slider.on_value_changed =self.update
        self.b_slider.on_value_changed =self.update
        self.speed_slider.on_value_changed = self.update
        self.spice_slider.on_value_changed = self.update
        self.wild_slider.on_value_changed = self.update
        self.movex_slider.on_value_changed = self.update
        self.movey_slider.on_value_changed = self.update
        self.hide_button = u.Button('Hide menu')
        self.hide_button.on_click = self.hide
        self.panel = u.WindowPanel(title="Visuals", content=[u.Text('Speed'), self.speed_slider, u.Text('Spice'), self.spice_slider, u.Text('Wildness'), self.wild_slider, u.Text('Move x, y'), self.movex_slider,self.movey_slider,
                                                                u.Text('R G B'), self.r_slider, self.g_slider, self.b_slider,self.hide_button,])
        self.panel.x = 0.5
        self.panel.y = 0
        self.hide()
        self.update()
        # super().__init__()

    def hide(self):
        self.panel.visible = False
        self.panel.enabled = False
    def show(self):
        self.panel.visible = True
        self.panel.enabled = True

    def update(self):
        self.animator.color = [self.r_slider.value, self.g_slider.value, self.b_slider.value]
        self.animator.speed = self.speed_slider.value
        self.animator.spice = self.spice_slider.value
        self.animator.wildness = self.wild_slider.value
        self.animator.movex = self.movex_slider.value
        self.animator.movey = self.movey_slider.value

CubeAnimator = cube_animator()

cubeHolder = CubeHolder(next_block(), CubeAnimator)

class playerWindow(u.Entity):
    def __init__(self,animator=None):
        self.centernum = 60
        self.animator = animator
        self.content = {}
        self.content['title'] = u.Text('Nothing playing'.center(self.centernum))
        self.content['artist'] = u.Text(' ')
        # self.content['bar'] = u.Slider(vertical=False, min=0, max=100)
        self.content['pause'] = u.Button('Toggle pause')
        self.content['pause'].on_click = self.pause
        self.content['next'] = u.Button('Next')
        self.content['next'].on_click = player.next
        self.content['previous'] = u.Button('Previous')
        self.content['previous'].on_click = player.previous
        self.content['volume'] = u.Slider(vertical=False, min=0, max=100, default=50)
        self.content['volume'].on_value_changed = lambda: player.set_volume(int(self.content['volume'].value))
        player.set_volume(int(self.content['volume'].value))
        self.content['searchbar'] = u.InputField()
        self.content['searchbutton'] = u.Button('Search song')
        self.content['searchbutton'].on_click = lambda: self.add_song()
        self.window = u.WindowPanel(title="pYTerm",
                                content = tuple(self.content.values())
                                )
        self.window.x = 0
        self.window.y = 0.3
        super().__init__()
    def update(self):
        if player.current_song != None and player.is_vlc_alive():
            # print('current song:',player.current_song)
            self.content['title'].text = player.current_song.title.center(self.centernum)
            self.content['artist'].text = ('by '+player.current_song.artist).center(self.centernum)
            if self.animator:
                self.animator.start()
        else:
            # print('no current song:',player.current_song)
            self.content['title'].text = 'Nothing playing'.center(self.centernum)
            self.content['artist'].text = ' '.center(self.centernum)
            if self.animator:
                self.animator.stop()
    def pause(self):
        if player.is_vlc_alive():
            if str(player.vlc_player.get_state()) == 'State.Paused':
                self.animator.unpause()
            else:
                self.animator.pause()
            threading.Thread(target=player.toggle_pause).start()

    def add_song(self):
        if self.content['searchbar'].text:
            threading.Thread(target=player.add_song, args=(self.content['searchbar'].text,)).start()
            self.content['searchbar'].text = ' '*50
            self.content['searchbar'].text = ' '

def main():
    app = u.Ursina()
    a = playerWindow(CubeAnimator)
    u.window.borderless = False
    # cam = u.EditorCamera()
    # b = u.prefabs.video_recorder.VideoRecorderUI()
    b = animationController(CubeAnimator)
    b.panel.x = 0.5
    b.panel.y = 0.45

    c = u.Button('Change visuals', x=0,y=-0.45, scale=u.Vec2(0.20,0.08))
    c.on_click = b.show
    app.run()

if __name__ == '__main__':
    main()
